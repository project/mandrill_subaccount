CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The Mandrill Subaccount module extends the Mandrill module by adding the ability
to assign outgoing emails to a subaccount.


REQUIREMENTS
------------
This module requires the following modules:
 * Mandrill

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » Configuration » Services »
   Mandrill » Subaccount

MAINTAINERS
-----------
Current maintainers:
 * Jason Richard Smith (jasonrichardsmith) - https://drupal.org/user/722872
